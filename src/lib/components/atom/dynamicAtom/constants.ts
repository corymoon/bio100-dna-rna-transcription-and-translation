import type { Coordinate } from '$lib/helpers/calculateCirclePoints'

export const imageSize = 700
export const electronRadius = (10 / 800) * imageSize
export const nucleusRadius = imageSize * 0.1
export const center: Coordinate = { x: imageSize / 2, y: imageSize / 2 }
export const shellElectrons = [2, 8, 18, 32, 50, 72, 98]
export const electronLimits = [0, 2, 10, 28, 60, 110, 182]

const _shellStep = imageSize * 0.1

const makeShellSteps = () => {
    const steps: number[] = []

    let val = nucleusRadius + _shellStep / 2
    for (let i = 1; i < 8; i++) {
        steps.push(val)
        val = val + _shellStep / 2
    }
    return steps
}

export const shellRadius = makeShellSteps()
