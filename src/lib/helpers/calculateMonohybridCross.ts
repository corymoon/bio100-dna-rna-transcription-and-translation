import _ from 'lodash'
import { sortAlleles } from './sortAlleles'

export const calculateMonohybridCross = (p1: string, p2: string) => {
    const p1S = p1[0],
        p1s = p1[1]
    const p2S = p2[0],
        p2s = p2[1]

    const cross = [
        ['', p1S, p1s],
        [p2S, sortAlleles(p1S, p2S), sortAlleles(p2S, p1s)],
        [p2s, sortAlleles(p1S, p2s), sortAlleles(p1s, p2s)],
    ]

    return cross
}

export const monohybridUniqueValues = (cross: string[][]) => _.uniq(monohybridAllValues(cross))

export const monohybridAllValues = (cross: string[][]) => {
    const values: string[] = []
    for (let i = 0; i < cross.length; i++) {
        const item = cross[i]
        for (let idx = 0; idx < item.length; idx++) {
            const genotype = item[idx]
            if (genotype.length === 4) {
                values.push(genotype)
            }
        }
    }
    return values
}
