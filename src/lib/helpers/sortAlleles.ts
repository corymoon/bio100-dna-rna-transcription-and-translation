export const sortAlleles = (allele1: string, allele2: string) => {
    let str = `${allele1}${allele2}`
    let split = str.split('')
    split = split.sort()
    str = split.join('')
    return str
}

export const makeAllelePairs = (parent1: string, parent2: string) => {
    const upperLeft = sortAlleles(parent1[0], parent2[0])
    const upperRight = sortAlleles(parent1[1], parent2[0])
    const lowerLeft = sortAlleles(parent1[0], parent2[1])
    const lowerRight = sortAlleles(parent1[1], parent2[1])

    return [upperLeft, upperRight, lowerLeft, lowerRight]
}

const colors = ['lime', 'pink', 'orange', 'green', 'yellow', 'purple', 'blue', 'cyan', 'red']

export const mapUniqueToColors = (uniquePairs: string[]) => {
    const arr: { pair: string; color: string }[] = []
    for (let i = 0; i < uniquePairs.length; i++) {
        const newObj = {
            pair: uniquePairs[i],
            color: colors[i],
        }
        // arr = [newObj, ...arr]
        arr.push(newObj)
    }
    return arr
}
