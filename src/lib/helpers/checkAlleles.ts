export const checkAlleles = (alleles: string) => {
    let good = true
    if (
        alleles.length !== 2 ||
        alleles === '' ||
        alleles === 'undefined' ||
        alleles === undefined
    ) {
        good = false
    } else {
        good = true
    }
    return good
}
