export const getPhenotypeFromGenotype = (
    genotype: string,
    domPhenotype: string,
    recPhenotype: string,
    domPhenotype2: string | undefined = undefined,
    recPhenotype2: string | undefined = undefined,
) => {
    const char1 = genotype[0]
    let phenotype: string

    // if (genotype.length === 2) {
    if (char1 === char1.toUpperCase()) {
        phenotype = domPhenotype
    } else {
        phenotype = recPhenotype
    }
    // } else
    if (genotype.length === 4 && domPhenotype2 !== undefined && recPhenotype2 !== undefined) {
        const char3 = genotype[2]
        if (char3 === char3.toUpperCase()) {
            phenotype += `, ${domPhenotype2}`
        } else {
            phenotype += `, ${recPhenotype2}`
        }
    }
    return phenotype
}
