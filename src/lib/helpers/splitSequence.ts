export const splitSequence = (sequence: string): string[] => {
    return sequence.match(/.{1,3}/g) as string[];
};
