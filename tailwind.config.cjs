/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ['./src/**/*.{html,js,svelte,ts}'],
    theme: {
        container: {
            center: true,
        },
        extend: {},
    },
    safelist: [
        'badge-secondary',
        'badge-accent',
        'badge-success',
        'text-purple-500',
        'text-pink-500',
        'text-orange-500',
        'text-green-500',
        'text-yellow-500',
        'text-red-500',
        'text-blue-500',
        'text-gray-500',
        'text-lime-500',
        'text-cyan-500'
    ],
    plugins: [require('daisyui')],
}
